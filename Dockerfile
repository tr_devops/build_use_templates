FROM alpine:latest
LABEL authors="Tobias Riazi <riazi.tobias@healthnow.org>"
LABEL description="Set Version - Python"

RUN apk update
RUN apk add python3 
RUN apk add python3-dev 
RUN apk add py3-pip
COPY requirements.txt .
RUN pip install -r requirements.txt
RUN rm requirements.txt

RUN mkdir app
WORKDIR /app
