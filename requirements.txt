certifi==2021.5.30
chardet==4.0.0
idna==2.10
requests==2.25.1
semantic-version==2.8.5
urllib3==1.26.5
